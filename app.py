import flask

app = flask.Flask(__name__)

@app.route('/', methods=['GET'])
def home():
    return "<h1> Demo Flask App </h1>"

if __name__ == "__main":
    app.run(debug=True, port=5050, host='localhost')


